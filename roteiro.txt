* Instalacoes:
	- Node.JS
	- VS Code
	- pip install virtualenv
	- https://automationpanda.com/tag/visual-studio-code/
	- Django rest framework:  https://www.django-rest-framework.org/

1) Dividir os projetos entre as pastas -backend e frontend
2) criar o projeto backend:
	- django-admin startproject djreact / renomear a pasta do projeto para src

3) instalar o react:
	- https://www.devmedia.com.br/como-instalar-o-node-js-npm-e-o-react-no-windows/40329

4) criar o app react:
	create-react-app gui

5) Configurar o projeto com o django-rest-framework
	- https://www.django-rest-framework.org/

6) Dentro da pasta gui instalar o axios
	npm install axios

7) Instalar o corsheaders para fazer a comunicação do react com a api
	https://github.com/adamchainz/django-cors-headers
	pip install django-cors-headers

8) Instalar o react-router-dom
	npm install --save react-router-dom
	